FROM flywheel/python:main.6fb83dc8 

SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]
WORKDIR /build

ENV XMEDCON_VERSION=0.22.0

# hadolint ignore=DL3003
RUN apt-get update; \
    apt-get install -y --no-install-recommends \
        build-essential=12.9 \
        libglib2.0-dev=2.66.8-1 \
        libpng-dev=1.6.37-3 \
        pkg-config=0.29.2-1 \
        zlib1g-dev=1:1.2.11.dfsg-2\
    ; \
    curl -fLSs "https://prdownloads.sourceforge.net/xmedcon/xmedcon-${XMEDCON_VERSION}.tar.gz" | tar -xz; \
    cd xmedcon-${XMEDCON_VERSION} || exit; \
    ./configure --disable-gui; \
    make; \
    make install; \
    apt-get remove -y \
        build-essential \
        libglib2.0-dev \
        libpng-dev \
        pkg-config \
        zlib1g-dev \
    ; \
    rm -rf /build/* /var/lib/apt/lists/*

RUN mkdir /data
WORKDIR /data

ENTRYPOINT ["/usr/local/xmedcon/bin/medcon"]
