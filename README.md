# (X)MedCon

Flywheel image for [(X)MedCon](https://xmedcon.sourceforge.io/Main/HomePage)

## Usage

### Base image

Start your image from `flywheel/python-xmedcon`:

```python
FROM flywheel/python-xmedcon
```

### Run

Mount your data to `/data` and run a medcon command.
Example for converting an ECAT7 to DICOM:

```bash
docker run -it --rm  
 -v /path/to/my/local/data/folder:/data 
 flywheel/python-xmedcon:latest 
 -f /data/<my-ecat7-filename>.v --split-frames -c dicom
```
